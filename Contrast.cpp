// ======================================================================
// IMPROC: Image Processing Software Package
// Copyright (C) 2016 by George Wolberg
//
// Contrast.cpp - Brightness/Contrast widget.
//
// Written by: George Wolberg, 2016
// ======================================================================

#include "MainWindow.h"
#include "Contrast.h"
#define DEFAULT_BRIGHTNESS 0
#define DEFAULT_CONTRAST 0

extern MainWindow *g_mainWindowP;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contrast::Contrast:
//
// Constructor.
//
Contrast::Contrast(QWidget *parent) : ImageFilter(parent)
{}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contrast::applyFilter:
//
// Run filter on the image, transforming I1 to I2.
// Overrides ImageFilter::applyFilter().
// Return 1 for success, 0 for failure.
//
bool
Contrast::applyFilter(ImagePtr I1, ImagePtr I2)
{
    
    // INSERT YOUR CODE HERE
    if(I1.isNull()) return 0;
    
    
    // apply filter
    double b, c;	// brightness, contrast parameters
    b = m_sliderB->value();
    c = m_sliderC->value();
    
    if(c >= 0){
        c = c/25.0 +1.0;
    }
    else{
        c = c/133.0 +1.0;
    }
    //if(b<0 || b> MXGRAY) return 0;
    //if(c<0 || c> MXGRAY) return 0;
    
    contrast(I1, b, c, I2);
    
    return 1;
}

//todo add enability and disablility feature of the menus unless the file is opened

//add negative and gamma feature, enhancement, quantization

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contrast::createGroupBox:
//
// Create group box for control panel.
//
QGroupBox*
Contrast::controlPanel()
{
	// init group box
	m_ctrlGrp = new QGroupBox("Contrast");
    QLabel *label = new QLabel;
    QLabel *labelb = new QLabel;

    label->setText(QString("cont"));
    
    // create slider
    m_sliderC = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_sliderC->setTickPosition(QSlider::TicksBelow);
    m_sliderC->setTickInterval(25);
    m_sliderC->setMinimum(-100);
    m_sliderC->setMaximum(100);
    m_sliderC->setValue  (0);
    
    // create spinbox
    m_spinBoxC = new QSpinBox(m_ctrlGrp);
    m_spinBoxC->setMinimum(-100);
    m_spinBoxC->setMaximum(100);
    m_spinBoxC->setValue  (0);
    
    // init signal/slot connections for Threshold
    connect(m_sliderC , SIGNAL(valueChanged(int)), this, SLOT(changeContrast (int)));
    connect(m_spinBoxC, SIGNAL(valueChanged(int)), this, SLOT(changeContrast (int)));
    
    // assemble dialog
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(  label  , 0, 0);
    layout->addWidget(m_sliderC , 0, 1);
    layout->addWidget(m_spinBoxC, 0, 2);
    
    //for brightness
    labelb->setText(QString("Brig"));
    
    // create slider
    m_sliderB = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_sliderB->setTickPosition(QSlider::TicksBelow);
    m_sliderB->setTickInterval(25);
    m_sliderB->setMinimum(-MXGRAY);
    m_sliderB->setMaximum(MXGRAY);
    m_sliderB->setValue  (0);
    
    // create spinbox
    m_spinBoxB = new QSpinBox(m_ctrlGrp);
    m_spinBoxB->setMinimum(-MXGRAY);
    m_spinBoxB->setMaximum(MXGRAY);
    m_spinBoxB->setValue  (0);
    
    // init signal/slot connections for Threshold
    connect(m_sliderB , SIGNAL(valueChanged(int)), this, SLOT(changeBright (int)));
    connect(m_spinBoxB, SIGNAL(valueChanged(int)), this, SLOT(changeBright (int)));
    layout->addWidget(  labelb  , 1, 0);
    layout->addWidget(m_sliderB , 1, 1);
    layout->addWidget(m_spinBoxB, 1, 2);
    
    // assign layout to group box
    m_ctrlGrp->setLayout(layout);


	// INSERT YOUR CODE HERE

	return(m_ctrlGrp);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// contrast:
//
// INSERT YOUR CODE HERE.
//
void
Contrast::contrast(ImagePtr I1, double brightness, double contrast, ImagePtr I2)
{
    
    IP_copyImageHeader(I1, I2);
    int w = I1->width();
    int h = I1->height();
    int total = w * h;
    
    int i, lut[MXGRAY];
    for(i=0; i<MXGRAY; ++i) //lut[i] = 0;
    {
        
        lut[i] = (double) CLIP(((i-128) * contrast) + 128 + brightness, 0, 255);
    }
    
    int type;
    ChannelPtr<uchar> p1, p2, endd;
    for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
        IP_getChannel(I2, ch, p2, type);
        for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
    }

}


void
Contrast::changeContrast(int contr)
{
    m_sliderC ->blockSignals(true);
    m_sliderC ->setValue    (contr );
    m_sliderC ->blockSignals(false);
    m_spinBoxC->blockSignals(true);
    m_spinBoxC->setValue    (contr );
    m_spinBoxC->blockSignals(false);
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}

void
Contrast::changeBright(int brig)
{
    m_sliderB ->blockSignals(true);
    m_sliderB ->setValue    (brig );
    m_sliderB ->blockSignals(false);
    m_spinBoxB->blockSignals(true);
    m_spinBoxB->setValue    (brig );
    m_spinBoxB->blockSignals(false);
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contrast::reset:
//
// Reset parameters.
//
void
Contrast::reset() {

    m_spinBoxB->setValue    (DEFAULT_BRIGHTNESS );
    m_sliderB->setValue  (DEFAULT_BRIGHTNESS);
    m_spinBoxC->setValue    (DEFAULT_CONTRAST );
    m_sliderC->setValue  (DEFAULT_CONTRAST);
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();


}
