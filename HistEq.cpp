//
//  HistEq.cpp
//  qip
//
//  Created by Indrajit on 5/15/16.
//
//

#include "MainWindow.h"
#include "HistEq.h"
#include <deque>
#include <algorithm>
#include <vector>


extern MainWindow *g_mainWindowP;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::Sharp:
//
// Constructor.
//
HistEq::HistEq(QWidget *parent) : ImageFilter(parent)
{}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::applyFilter:
//
// Run filter on the image, transforming I1 to I2.
// Overrides ImageFilter::applyFilter().
// Return 1 for success, 0 for failure.
//
bool
HistEq::applyFilter(ImagePtr I1, ImagePtr I2)
{
    // error checking
    if(I1.isNull()) return 0;
    histeq(I1, I2);
    
    return 1;
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::controlPanel:
//
// Create group box for control panel.
//
QGroupBox*
HistEq::controlPanel()
{
    
    m_ctrlGrp = new QGroupBox("Histogram Operation");
    
    // assemble dialog
    QGridLayout *layout = new QGridLayout;
    
    // assign layout to group box
    m_ctrlGrp->setLayout(layout);
    
    return(m_ctrlGrp);
}



void
HistEq::histeq(ImagePtr I1, ImagePtr I2) {
    IP_copyImageHeader(I1, I2);
    int i, R;
    int left[MXGRAY], width[MXGRAY];
    //uchar *in, *out;
    long total, Hsum, Havg, histo[MXGRAY];
    
    total = (long) I1->width()* I1->height();
    /* init I2 dimensions and buffer */
    //I2->width() = I1->width();
    //I2->height() = I1->height();
    //I2->image() = (uchar *) malloc(total);
    /* init input and output pointers */
    //in = I1->image;
    //out = I2->image;
    int type;
    ChannelPtr<uchar> in, out, endd;
    for(int ch = 0; IP_getChannel(I1, ch, in, type); ch++) {
        IP_getChannel(I2, ch, out, type);
        //for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];

    
    /* input image buffer */
    /* output image buffer */
    /* compute histogram */
    for(i=0; i<MXGRAY; i++)
        histo[i] = 0; /* clear histogram */
    
    
    for(i=0; i<total; i++)
    {
        histo[in[i]]++;
        //std::cout<<i<<") pixel value "<<in[i]<<std::endl;

        //std::cout<<"histogram "<<histo[in[i]]<<std::endl;
    }
   
    R = 0;
    Hsum = 0;
    Havg = total / MXGRAY;
//    /* eval histogram */
//    /* right end of interval */
//        /* cumulative value for interval */
//        /* interval value for uniform histogram */
//
//
//    
//    /* evaluate remapping of all input gray levels;
//     * Each input gray value maps to an interval of valid output values. * The endpoints of the intervals are left[] and left[]+width[].
//     */
    for(i=0; i<MXGRAY; i++) {
        left[i] = R; /* left end of interval */
        Hsum += histo[i]; /* cum. interval value */
        while(Hsum>Havg && R<MXGRAY-1) {
            Hsum -= Havg;
            R++;
        }
        width[i] = R - left[i] + 1;
    }
//    /* adjust Hsum */ /* update right end */
//    /* width of interval */
//    /* visit all input pixels and remap intensities */
    for(i=0; i<total; i++) {
        if(width[in[i]] == 1)
            out[i] = left[in[i]];
        else {
            /* in[i] spills over into width[] possible values */ /* randomly pick from 0 to width[i] */
            R = ((rand()&0x7fff)*width[in[i]])>>15; /* 0 <= R < width */
            out[i] = left[in[i]] + R;
        }
    }
    }
}






// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::reset:
//
// Reset parameters.
//
void
HistEq::reset() { }


