//
//  HistEq.hpp
//  qip
//
//  Created by Indrajit on 5/15/16.
//
//

#ifndef HistEq_h
#define HistEq_h
#include <stdio.h>
#include "ImageFilter.h"
#include <deque>

class HistEq : public ImageFilter {
    Q_OBJECT
    
public:
    HistEq	(QWidget *parent = 0);		// constructor
    
    
    
    
    
    QGroupBox*	controlPanel	();		// create control panel
    bool		applyFilter(ImagePtr, ImagePtr);// apply filter to input to init output
    void		reset		();		// reset parameters
    
protected:
    //void blur_it(ImagePtr , int , int , ImagePtr);
    
    //uint16_t* pad_rows(ChannelPtr<uchar>, int,  int, int);
    //std::vector<uint16_t>  pad_rows(ChannelPtr<uchar>, int,  int, int);
    void histeq(ImagePtr, ImagePtr);
    //void calculate_median(std::deque<std::vector<uint16_t> >, ChannelPtr<uchar>, int, int, int, int);
    //void Med(ImagePtr, int, int, double, ImagePtr);
    //void drag_both(int);
    
    protected slots:
    //void changeblur(int);
    //void changeblury (int);
    //void changeSharpness(int);
    //void check_merge_state(int);
    
    //void Sharpen(ImagePtr, double , ImagePtr);
    //void changeMedian(int);
    
    
    
private:
    // my_blur controls
//    QSlider		*m_slider ;	// my_blur sliders
//    QSlider     *m_slidery;
//    QSpinBox	*m_spinBox;	// my_blur spin boxes
//    QSpinBox    *m_spinBoxy;
//    QSlider     *m_sliderS;
//    QSpinBox    *m_spinBoxS;
    
    // label for Otsu my_blurs
    QLabel		*m_label;	// Label for printing Otsu my_blurs
    
    // widgets and groupbox
    QGroupBox	*m_ctrlGrp;	// Groupbox for panel
    //QCheckBox *checkbox_merge;
    
};


#endif /* HistEq_hpp */
