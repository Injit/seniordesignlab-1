//
//  Median.cpp
//  qip
//
//  Created by Indrajit on 4/16/16.
//
//
#include "MainWindow.h"
#include "Median.h"
#include <deque>
#include <algorithm>
#include <vector>


extern MainWindow *g_mainWindowP;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::Sharp:
//
// Constructor.
//
Median::Median(QWidget *parent) : ImageFilter(parent)
{}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::applyFilter:
//
// Run filter on the image, transforming I1 to I2.
// Overrides ImageFilter::applyFilter().
// Return 1 for success, 0 for failure.
//
bool
Median::applyFilter(ImagePtr I1, ImagePtr I2)
{
    // error checking
    if(I1.isNull()) return 0;
    
    // get My_blur value
    int xsz = m_slider->value();
    int ysz = m_slidery->value();
    double avrg_nbrs = m_sliderS->value();
    
    
    
    // error checking
    if(xsz < 0 || xsz > MXGRAY || ysz < 0 || ysz > MXGRAY) return 0;
    
    // apply filter
    Med(I1, xsz, ysz, avrg_nbrs, I2);
    
    return 1;
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::controlPanel:
//
// Create group box for control panel.
//
QGroupBox*
Median::controlPanel()
{
    
    
    
    m_ctrlGrp = new QGroupBox("Median");
    checkbox_merge = new QCheckBox("xsz=ysz", this);
    checked_histogram = new QCheckBox("Histo Median", this);
    // init widgets
    // create label[i]
    QLabel *label = new QLabel;
    QLabel *labely = new QLabel;
    label->setText(QString("xsz"));
    labely->setText(QString("ysz"));
    
    
    // create slider
    m_slider = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_slider->setTickPosition(QSlider::TicksBelow);
    m_slider->setTickInterval(25);
    m_slider->setMinimum(1);
    m_slider->setMaximum(MXGRAY);
    //m_slider->setValue  (MXGRAY>>1);
    m_slider->setValue  (1);
    
    
    // create spinbox
    m_spinBox = new QSpinBox(m_ctrlGrp);
    m_spinBox->setMinimum(1);
    m_spinBox->setMaximum(MXGRAY);
    //m_spinBox->setValue  (MXGRAY>>1);
    m_spinBox->setValue  (1);
    
    
    // init signal/slot connections for My_blur
    connect(m_slider , SIGNAL(valueChanged(int)), this, SLOT(changeblur (int)));
    connect(m_spinBox, SIGNAL(valueChanged(int)), this, SLOT(changeblur (int)));
    connect(checkbox_merge, SIGNAL(stateChanged(int)), this, SLOT(check_merge_state (int)));
    connect(checked_histogram, SIGNAL(stateChanged(int)), this, SLOT(histogram_checked (int)));
    
    
    /////////////
    m_slidery = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_slidery->setTickPosition(QSlider::TicksBelow);
    m_slidery->setTickInterval(25);
    m_slidery->setMinimum(1);
    m_slidery->setMaximum(MXGRAY);
    //m_slidery->setValue  (MXGRAY>>1);
    m_slidery->setValue  (1);
    
    
    // create spinbox
    m_spinBoxy = new QSpinBox(m_ctrlGrp);
    m_spinBoxy->setMinimum(1);
    m_spinBoxy->setMaximum(MXGRAY);
    //m_spinBoxy->setValue  (MXGRAY>>1);
    m_spinBoxy->setValue  (1);
    
    
    // init signal/slot connections for My_blur
    connect(m_slidery , SIGNAL(valueChanged(int)), this, SLOT(changeblury (int)));
    connect(m_spinBoxy, SIGNAL(valueChanged(int)), this, SLOT(changeblury (int)));
    connect(checkbox_merge, SIGNAL(stateChanged(int)), this, SLOT(check_merge_state (int)));
    connect(checked_histogram, SIGNAL(stateChanged(int)), this, SLOT(histogram_checked (int)));

    
    QLabel *labelS = new QLabel;
    labelS->setText(QString("avg_nbrs"));
    
    // create slider
    m_sliderS = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_sliderS->setTickPosition(QSlider::TicksBelow);
    m_sliderS->setTickInterval(25);
    m_sliderS->setMinimum(0);
    m_sliderS->setMaximum(MXGRAY);
    m_sliderS->setValue  (0);
    
    // create spinbox
    m_spinBoxS = new QSpinBox(m_ctrlGrp);
    m_spinBoxS->setMinimum(0);
    m_spinBoxS->setMaximum(MXGRAY);
    m_spinBoxS->setValue  (0);
    
    // init signal/slot connections for Sharp
    connect(m_sliderS , SIGNAL(valueChanged(int)), this, SLOT(changeSharpness (int)));
    connect(m_spinBoxS, SIGNAL(valueChanged(int)), this, SLOT(changeSharpness (int)));
    
    // assemble dialog
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(  label  , 0, 0);
    layout->addWidget(m_slider , 0, 1);
    layout->addWidget(m_spinBox, 0, 2);
    layout->addWidget(  labely  , 1, 0);
    layout->addWidget(m_slidery , 1, 1);
    layout->addWidget(m_spinBoxy, 1, 2);
    layout->addWidget(checkbox_merge, 2, 0);
    layout->addWidget(  labelS  , 3, 0);
    layout->addWidget(m_sliderS , 3, 1);
    layout->addWidget(m_spinBoxS, 3, 2);
    layout->addWidget(checked_histogram, 4, 0);
    
    // assign layout to group box
    m_ctrlGrp->setLayout(layout);
    
    return(m_ctrlGrp);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::changeThr:
//
// Slot to process change in thr caused by moving the slider.
//
void
Median::changeSharpness(int Shr)
{
    m_sliderS ->blockSignals(true);
    m_sliderS ->setValue    (Shr );
    m_sliderS ->blockSignals(false);
    m_spinBoxS->blockSignals(true);
    m_spinBoxS->setValue    (Shr );
    m_spinBoxS->blockSignals(false);
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::Sharp:
//
// Sharp I1 using the 2-level mapping shown below.  Output is in I2.
// val<thr: 0;	 val >= thr: MaxGray (255)
//! \brief	Sharp I1 using the 3-level mapping shown below.
//! \details	Output is in I2. val<t1: g1; t1<=val<t2: g2; t2<=val: g3
//! \param[in]	I1  - Input image.
//! \param[in]	thr - Sharp.
//! \param[out]	I2  - Output image.
//


void
Median::Med(ImagePtr I1, int xsz, int ysz, double avg_nbrs, ImagePtr I2){
    IP_copyImageHeader(I1, I2);
    //std::deque<uint16_t*> queue_of_rows;
    std::deque<std::vector<uint16_t> >  queue_of_rows;

    int w = I1->width();
    int h = I1->height();
    int total = w * h;
    
    if(xsz >w){
        xsz = w;
    }
    if(ysz >h){
        ysz = h;
    }
    if(avg_nbrs>(xsz*ysz)/2){
        
        avg_nbrs = (xsz*ysz/2)-1;

    }
    
    int type;
    ChannelPtr<uchar> p1, p2,p3, endd;//pointers for src, temp and destination
    
    
    for (int ch = 0; IP_getChannel(I1, ch, p1, type); ch++){//p1 is now assigned as source image pointer
        
        IP_getChannel(I2, ch, p2, type);
        if(xsz ==1 && ysz == 1){
            for(int init =0; init<total; init++){
                *p2 = *p1;
                p1++;
                p2++;
            }
            return;
        }
        
        
        //add padding on side and add the top padding and replicates the pixel value to the padded row from the first row of original image
        int i = 0;
        for (; i < (ysz-1)/2; i++){ // do top padding
            queue_of_rows.push_back(pad_rows(p1, w, xsz, 1));//pad_rows add padding on left and right based on xsz and assigns pixel of first for left padding and last for right padding

        }
        //padded first row plus additional padded rows upto ysz are push_back to the deque. After this we have all rows need for processing the first row of the output
        for(; i<ysz; i++){
            queue_of_rows.push_back(pad_rows(p1, w, xsz, 1));
            p1+=w; // go to next row
        }
        //p1 -= 1;
        //this adds the padded row untill
        for(; i<h; i++)
        {
            calculate_median(queue_of_rows, p2, avg_nbrs, xsz, ysz, w);//takes the padded rows of ysz, calculates the median and puts it to the p2 which is the pointer for the output
            queue_of_rows.pop_front();//removes the very top row
            queue_of_rows.push_back(pad_rows(p1, w, xsz, 1));//new row is added at lower level
            
            p1 += w;//takes to one row down pixel adding offset of the width in src image
            p2 += w;//takes to one row down pixel in output image
        }
        p1 -= w;//returns back to the last row so that the pixel of last row can be replicated to the bottom padding
        //p2 -= w;
        
        for(; i< h+(ysz-1); i++){
            calculate_median(queue_of_rows, p2, avg_nbrs, xsz, ysz, w);
            queue_of_rows.pop_front();
            queue_of_rows.push_back(pad_rows(p1, w, xsz, 1));//last row pixel of src image are replicated to the lower padding
            p2 += w;
        }
        queue_of_rows.clear();
        
    }

}

void
Median::calculate_median(std::deque<std::vector<uint16_t> > rows, ChannelPtr<uchar> p2, int avg_nbrs, int xsz, int ysz, int w){
    
    
    std::vector<uint16_t> neighbors;
    
    //outer loop to move the kernel horizontally
    for(int k = 0; k< w; k++){
        
        int sum = 0;
        
        
        for(int i = k; i<xsz+k; i++){
            
            for(int j = 0; j<ysz; j++){
                
                //storing the value of the kernel into the neighbors vector so that they can be sorted and the median can be determined
                neighbors.push_back(rows[j][i]);
                
                
            }
            
        }
        
        //calculate histogram median
        if(checked_histogram->isChecked()){

            
            int histo[MXGRAY];
            
            for(int k=0; k<MXGRAY; k++)
                histo[k] = 0; /* clear histogram */
            
            int i;
            for(i= 0; i<xsz*ysz; i++)
            {
                histo[neighbors[i]]++;//create histogram of the pixel values of the kernel (pixel value are from 0 to 255)
                //std::cout<<neighbors[i]<<std::endl;
                //std::cout<<"histo  "<<histo[neighbors[i]]<<std::endl;
            }
            
            for(int j = 0; j<MXGRAY; j++){
                sum += histo[j];//cumulative frequency
                //std::cout<<j <<") sum   "<<sum<<std::endl;

                
                
                if(sum >= (xsz*ysz)/2){//cumulative reaches half of the kernel size
                    *p2 = j;//CLIP(j, 0, MXGRAY-1);//assign the kernel pixel to output
                    p2++;
                    break;
                }
            }
            
        }


        else{
        //sorts the neighbors vector
            std::sort(neighbors.begin(), neighbors.end());
        

            //based on avg neighbors value the sum withing that range is calculated
            for(int start = neighbors.size()/2-avg_nbrs; start<=(int)(neighbors.size()/2 + avg_nbrs); start++){
            
                //std::cout<<start<<"*****"<<std::endl;
            
                sum += neighbors[start];
            
            
                //std::cout<<sum<<" ";
                
                //neighbors.clear();
            
            }

            //*p2 = sum/(2 * avg_nbrs +1);
            *p2 = CLIP(sum/((2 * avg_nbrs) + 1), 0, MXGRAY-1);

            p2++;
        }
        neighbors.clear();//this clears up the vector for another fresh data that comes from next iteration
    }
    
}


//Function to add left and right padding to each individual rows and returns in the form of vector of pixels
std::vector<uint16_t>
Median::pad_rows(ChannelPtr<uchar> p1, int w,  int xsz, int increment){
        

        //vector to hold the each row with padding
        //uint16_t * tempV;
        //tempV= (uint16_t *) malloc(sizeof(uint16_t) * (wh + (xysz-1)));
    
    
    //vector initialize to the new row size that has padding this hold row with padding on left and right based on xsz obtained
    std::vector<uint16_t>padded_row(w+(xsz-1));
    
    //if xsz is 1 
    if(xsz == 1){

        for(int row = 0; row<w; row++){
            padded_row[row] = *p1;
            p1++;
        }
        return padded_row;
    }
    
    
        int pad;
        for(pad = 0 ; pad < ((xsz-1)/2) && pad<(w + (xsz-1)); pad++){
            padded_row[pad] = *p1; //initial space gets filled with first pixel value of the src image
            padded_row[pad + (w+((xsz-1))/2)] = *(p1 + w -1);
        }
        //pad--;
        for(;  pad < w+ (xsz-1)/2 ; pad++, p1+=increment)
            padded_row[pad] = *p1;//pixel for the center
        
//        p1-=increment;
//        
//        for(;  pad <w+ (xsz-1); pad++)
//            padded_row[pad] = *p1;//pixel for the last padding

    return padded_row;
    
}


void
Median::check_merge_state(int){
    //apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    //display output
    g_mainWindowP->displayOut();
}

void
Median::histogram_checked(int){
    //apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    if(checked_histogram->isChecked()){
        m_sliderS->blockSignals(true);
        m_sliderS->setEnabled(false);
        m_sliderS->blockSignals(false);
        
        m_spinBoxS->blockSignals(true);
        m_spinBoxS->setEnabled(false);
        m_spinBoxS->blockSignals(false);
        
    }
    else{
        m_sliderS->blockSignals(true);
        m_sliderS->setEnabled(true);
        m_sliderS->blockSignals(false);
        
        m_spinBoxS->blockSignals(true);
        m_spinBoxS->setEnabled(true);
        m_spinBoxS->blockSignals(false);    }


    //display output
    g_mainWindowP->displayOut();
}

void
Median::changeblur(int blr)
{
    bool combine = checkbox_merge->isChecked();
    
    if(combine){
        drag_both(blr);
    }
    else{
        
        
        if(blr%2 ==0 && m_slider->value()>m_spinBox->value()){
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr-1);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr-1);
            m_spinBox->blockSignals(false);
            
            
            
        }
        else if (blr%2 ==0 && m_slider->value()<m_spinBox->value()){
            
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr+1);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr+1);
            m_spinBox->blockSignals(false);
        }
        else{
            
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr);
            m_spinBox->blockSignals(false);
        }
        
    }
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}


void
Median::changeblury(int blr)
{
    
    bool combine = checkbox_merge->isChecked();
    
    if(combine){
        drag_both(blr);
    }
    else{
        
        if(blr%2==0 && m_slidery->value()>m_spinBoxy->value()){
            
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr-1 );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr-1);
            m_spinBoxy->blockSignals(false);
        }
        else if(blr%2==0 && m_slidery->value()<m_spinBoxy->value()){
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr+1 );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr+1 );
            m_spinBoxy->blockSignals(false);
        }
        else{
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr );
            m_spinBoxy->blockSignals(false);
        }
        
    }
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}



void
Median::drag_both(int blr){
    if(blr%2 ==0 && (m_slider->value()<m_spinBox->value() || m_slidery->value()<m_spinBoxy->value())){
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr+1);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr+1);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr+1 );
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr+1 );
        m_spinBoxy->blockSignals(false);
    }
    else if(blr%2 ==0 && (m_slider->value()>m_spinBox->value() || m_slidery->value()>m_spinBoxy->value())){
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr-1);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr-1);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr-1);
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr-1);
        m_spinBoxy->blockSignals(false);
    }
    else{
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr);
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr);
        m_spinBoxy->blockSignals(false);
    }
}




// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sharp::reset:
//
// Reset parameters.
//
void
Median::reset() {
    m_spinBox->setValue    (1 );
    m_slider->setValue  (1);
    m_spinBoxy->setValue    (1 );
    m_slidery->setValue  (1);
    m_spinBoxS->setValue    (0);
    m_sliderS->setValue  (0);
}

