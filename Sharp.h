//
//  Sharp.hpp
//  qip
//
//  Created by Indrajit on 4/16/16.
//
//

#ifndef Sharp_h
#define Sharp_h

#include "ImageFilter.h"

class Sharp : public ImageFilter {
    Q_OBJECT
    

public:
    Sharp	(QWidget *parent = 0);		// constructor
    QGroupBox*	controlPanel	();		// create control panel
    bool		applyFilter(ImagePtr, ImagePtr);// apply filter to input to init output
    void		reset		();		// reset parameters
    
protected:
    void blur_it(ImagePtr , int , int , ImagePtr);
    
    protected slots:
    void changeblur(int);
    void changeblury (int);
    void passtotem(ChannelPtr<uchar>, int, int, int, ChannelPtr<uchar>);
    void check_merge_state(int);
    void drag_both(int);
    void changeSharpness(int);
    void Sharpen(ImagePtr, double , ImagePtr);
private:
    // my_blur controls
    QSlider		*m_slider ;	// my_blur sliders
    QSlider     *m_slidery;
    QSpinBox	*m_spinBox;	// my_blur spin boxes
    QSpinBox    *m_spinBoxy;
    QSlider     *m_sliderS;
    QSpinBox    *m_spinBoxS;
    
    // label for Otsu my_blurs
    QLabel		*m_label;	// Label for printing Otsu my_blurs
    
    // widgets and groupbox
    QGroupBox	*m_ctrlGrp;	// Groupbox for panel
    QCheckBox *checkbox_merge;
    
};


#endif	// my_blur_H

