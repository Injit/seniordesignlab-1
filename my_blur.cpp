
#include "MainWindow.h"
#include "my_blur.h"
#include <iostream>
extern MainWindow *g_mainWindowP;



My_blur::My_blur(QWidget *parent)
    : ImageFilter(parent)
{}




bool
My_blur::applyFilter(ImagePtr I1, ImagePtr I2)
{
    // error checking
    if(I1.isNull()) return 0;
    
    // get My_blur value
    int xsz = m_slider->value();
    int ysz = m_slidery->value();
    
    
    
    // error checking
    if(xsz < 0 || xsz > MXGRAY || ysz < 0 || ysz > MXGRAY) return 0;
//    else{
//        if(xsz%2 == 0) xsz += 1;
//        if(ysz%2 ==0) ysz +=1;
//        
//    }
    
    // apply filter
    blur_it(I1, xsz, ysz, I2);
    
    return 1;
}



QGroupBox*
My_blur::controlPanel()
{
    // init group box
    m_ctrlGrp = new QGroupBox("Blur");
    checkbox_merge = new QCheckBox("xsz=ysz", this);
    // init widgets
    // create label[i]
    QLabel *label = new QLabel;
    QLabel *labely = new QLabel;
    label->setText(QString("xsz"));
    labely->setText(QString("ysz"));

    
    // create slider
    m_slider = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_slider->setTickPosition(QSlider::TicksBelow);
    m_slider->setTickInterval(25);
    m_slider->setMinimum(1);
    m_slider->setMaximum(MXGRAY);
    //m_slider->setValue  (MXGRAY>>1);
    m_slider->setValue  (1);

    
    // create spinbox
    m_spinBox = new QSpinBox(m_ctrlGrp);
    m_spinBox->setMinimum(1);
    m_spinBox->setMaximum(MXGRAY);
    //m_spinBox->setValue  (MXGRAY>>1);
    m_spinBox->setValue  (1);

    
    // init signal/slot connections for My_blur
    connect(m_slider , SIGNAL(valueChanged(int)), this, SLOT(changeblur (int)));
    connect(m_spinBox, SIGNAL(valueChanged(int)), this, SLOT(changeblur (int)));
    connect(checkbox_merge, SIGNAL(stateChanged(int)), this, SLOT(check_merge_state (int)));


    /////////////
    m_slidery = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_slidery->setTickPosition(QSlider::TicksBelow);
    m_slidery->setTickInterval(25);
    m_slidery->setMinimum(1);
    m_slidery->setMaximum(MXGRAY);
    //m_slidery->setValue  (MXGRAY>>1);
    m_slidery->setValue  (1);

    
    // create spinbox
    m_spinBoxy = new QSpinBox(m_ctrlGrp);
    m_spinBoxy->setMinimum(1);
    m_spinBoxy->setMaximum(MXGRAY);
    //m_spinBoxy->setValue  (MXGRAY>>1);
    m_spinBoxy->setValue  (1);

    
    // init signal/slot connections for My_blur
    connect(m_slidery , SIGNAL(valueChanged(int)), this, SLOT(changeblury (int)));
    connect(m_spinBoxy, SIGNAL(valueChanged(int)), this, SLOT(changeblury (int)));
    connect(checkbox_merge, SIGNAL(stateChanged(int)), this, SLOT(check_merge_state (int)));

    
    
    /////////////
    
    // assemble dialog
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(  label  , 0, 0);
    layout->addWidget(m_slider , 0, 1);
    layout->addWidget(m_spinBox, 0, 2);
    layout->addWidget(  labely  , 1, 0);
    layout->addWidget(m_slidery , 1, 1);
    layout->addWidget(m_spinBoxy, 1, 2);
    layout->addWidget(checkbox_merge, 2, 0);

    
    // assign layout to group box
    m_ctrlGrp->setLayout(layout);
    
    return(m_ctrlGrp);
}

void
My_blur::check_merge_state(int){
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}


// My_blur::changeThr:
//
// Slot to process change in thr caused by moving the slider.
//
void
My_blur::changeblur(int blr)
{
    
    bool combine = checkbox_merge->isChecked();

    if(combine){
        drag_both(blr);
    }
    else{
        if(blr%2 ==0 && m_slider->value()>m_spinBox->value()){
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr-1);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr-1);
            m_spinBox->blockSignals(false);
            
            
            
        }
        else if (blr%2 ==0 && m_slider->value()<m_spinBox->value()){
            
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr+1);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr+1);
            m_spinBox->blockSignals(false);
        }
        else{
            
            m_slider ->blockSignals(true);
            m_slider ->setValue    (blr);
            m_slider ->blockSignals(false);
            m_spinBox->blockSignals(true);
            m_spinBox->setValue    (blr);
            m_spinBox->blockSignals(false);
        }
    }

    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}



void
My_blur::changeblury(int blr)
{
    
    bool combine = checkbox_merge->isChecked();
    
    if(combine){
        drag_both(blr);
    }
    else{
        
        
        if(blr%2==0 && m_slidery->value()>m_spinBoxy->value()){
            
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr-1 );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr-1);
            m_spinBoxy->blockSignals(false);
        }
        else if(blr%2==0 && m_slidery->value()<m_spinBoxy->value()){
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr+1 );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr+1 );
            m_spinBoxy->blockSignals(false);
        }
        else{
            
            m_slidery ->blockSignals(true);
            m_slidery ->setValue    (blr );
            m_slidery ->blockSignals(false);
            m_spinBoxy->blockSignals(true);
            m_spinBoxy->setValue    (blr );
            m_spinBoxy->blockSignals(false);
        }
        
    }
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}


void
My_blur::drag_both(int blr){
    if(blr%2 ==0 && (m_slider->value()<m_spinBox->value() || m_slidery->value()<m_spinBoxy->value())){
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr+1);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr+1);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr+1 );
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr+1 );
        m_spinBoxy->blockSignals(false);
    }
    else if(blr%2 ==0 && (m_slider->value()>m_spinBox->value() || m_slidery->value()>m_spinBoxy->value())){
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr-1);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr-1);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr-1);
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr-1);
        m_spinBoxy->blockSignals(false);
    }
    else{
        m_slider ->blockSignals(true);
        m_slider ->setValue    (blr);
        m_slider ->blockSignals(false);
        m_spinBox->blockSignals(true);
        m_spinBox->setValue    (blr);
        m_spinBox->blockSignals(false);
        m_slidery ->blockSignals(true);
        m_slidery ->setValue    (blr);
        m_slidery ->blockSignals(false);
        m_spinBoxy->blockSignals(true);
        m_spinBoxy->setValue    (blr);
        m_spinBoxy->blockSignals(false);
    }

}





// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// My_blur::My_blur:
//
// My_blur I1 using the 2-level mapping shown below.  Output is in I2.
// val<thr: 0;	 val >= thr: MaxGray (255)
//! \brief	My_blur I1 using the 3-level mapping shown below.
//! \details	Output is in I2. val<t1: g1; t1<=val<t2: g2; t2<=val: g3
//! \param[in]	I1  - Input image.
//! \param[in]	thr - My_blur.
//! \param[out]	I2  - Output image.
//
void
My_blur::blur_it(ImagePtr I1, int xsz, int ysz, ImagePtr I2) {
    IP_copyImageHeader(I1, I2);


    int w = I1->width();
    int h = I1->height();
    //int total = w * h;
    
    if(xsz >w){
        xsz = w;
    }
    if(ysz >h){
        ysz = h;
    }
    
    int type;
    ChannelPtr<uchar> p1, p2,p3, endd;//pointers for src, temp and destination
    
    
    for (int ch = 0; IP_getChannel(I1, ch, p1, type); ch++){//p1 is now assigned as source image pointer
        
        IP_getChannel(I2, ch, p2, type); // gets channle 0 1 or 2 (r, g ,b) array
        for (int i = 0; i < h; ++i){ // go from top row to bottom
            // now get 1d blur of pixel width w, step 1, and neighborhood xsz
            passtotem(p1, w, xsz, 1, p2);
            p1+=w; // go to next row
            p2+=w;
        }
        
        // reinitialize pointer to point to output picture to do columns
        IP_getChannel(I2, ch, p2, type); // gets channle 0 1 or 2 (r, g ,b) array
        
        // get pointer for output
        IP_getChannel(I2, ch, p3, type); // gets channle 0 1 or 2 (r, g ,b) array
        
        for (int i = 0; i < w; ++i){ // go from 1st col to last
            // now get 1d blur of pixel width h, step w, and neighborhood ysz
            passtotem(p2, h, ysz, w, p3);
            p2++; // go to next row
            p3++;
        }
        
    }
    

    
}
void
My_blur::passtotem(ChannelPtr<uchar> p1, int wh,  int xysz, int increment, ChannelPtr<uchar> p2){

    
    if(xysz == 1){
        for(int i=0;i<wh;i++){
            *p2=*p1;
            p2+=increment;
            p1+=increment;
        }
        return;
    }
   
    
    
    //vector to hold the each row with padding
//    uint16_t * tempV;
//    tempV= (uint16_t *) malloc(sizeof(uint16_t) * (wh + (xysz-1)));
    std::vector<int>tempV(wh+(xysz-1));
    
//    for(int i = 0; i<(wh + (xysz-1)); i++){
//        //tempV.push_back(0);
//        tempV[i] = 0;
//    }

    
    int pad;
    for(pad = 0 ; pad < ((xysz-1)/2) && pad<(wh + (xysz-1)); pad ++){
        tempV[pad] = *p1; //front padding gets filled with first pixel value of the src image
        tempV[pad + (wh+((xysz-1))/2)] = *(p1 + wh -1);//last padding gets the last pixel value of the original image
    }

    //pixel for the center is just copied as it was
    for(;  pad < wh+ (xysz-1)/2 ; pad++, p1+=increment)
        tempV[pad] = *p1;
    
//    p1-=increment;
//  
//    for(;  pad <wh+ (xysz-1); pad++)
//        tempV[pad] = *p1;//pixel for the center
    

    int sum = 0;
    
    int x=0;
    for(; x <xysz ; x++ ){
        sum += tempV[x];
    }
    //*p2 = sum/xysz;
    //p2 += increment;

    for(; x < wh + (xysz-1); x++ ){
        //sum += (tempV[x + ((xysz-1) + increment)] - tempV[x - (xysz-1)]);
        *p2 = sum/xysz;
        p2 += increment;
        sum += (tempV[x] - tempV[x - xysz]);

        //*p2 = sum/xysz;
        //p2 += increment;

    }
    //free(tempV);
    
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// My_blur::reset:
//
// Reset parameters.
//
void
My_blur::reset() {
    m_spinBox->setValue    (1 );
    m_slider->setValue  (1);
    m_spinBoxy->setValue    (1 );
    m_slidery->setValue  (1);
    
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();

}

