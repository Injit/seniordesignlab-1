#ifndef my_blur_h
#define my_blur_h

#include "ImageFilter.h"

class My_blur : public ImageFilter {
    Q_OBJECT
    
public:
    My_blur	(QWidget *parent = 0);		// constructor
    QGroupBox*	controlPanel	();		// create control panel
    bool		applyFilter(ImagePtr, ImagePtr);// apply filter to input to init output
    void		reset		();		// reset parameters
    
protected:
    void blur_it(ImagePtr , int , int , ImagePtr);
    
protected slots:
    void changeblur(int);
    void changeblury (int);
    void passtotem(ChannelPtr<uchar>, int, int, int, ChannelPtr<uchar>);
    void check_merge_state(int);
    void drag_both(int);
    
private:
    // my_blur controls
    QSlider		*m_slider ;	// my_blur sliders
    QSlider     *m_slidery;
    QSpinBox	*m_spinBox;	// my_blur spin boxes
    QSpinBox    *m_spinBoxy;
    
    // label for Otsu my_blurs
    QLabel		*m_label;	// Label for printing Otsu my_blurs
    
    // widgets and groupbox
    QGroupBox	*m_ctrlGrp;	// Groupbox for panel
    QCheckBox *checkbox_merge;

};


#endif	// my_blur_H
