//
//  quantization.cpp
//  qip
//
//  Created by Indrajit on 3/21/16.
//
//
#include "MainWindow.h"
#include "quantization.h"
extern MainWindow *g_mainWindowP;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contrast::Contrast:
//
// Constructor.
//
Quantization::Quantization(QWidget *parent) : ImageFilter(parent)
{}

bool
Quantization::applyFilter(ImagePtr I1, ImagePtr I2)
{
    
    // INSERT YOUR CODE HERE
    if(I1.isNull()) return 0;
    
    
    // apply filter
    int q;	// brightness, contrast parameters
    q = m_sliderQ->value();
    
    quantiz(I1, q, I2);
    
    return 1;
}


QGroupBox*
Quantization::controlPanel()
{
    // init group box
    m_ctrlGrp = new QGroupBox("Quantization");
    checkbox_dithering = new QCheckBox("Dither", this);
    
    // init widgets
    // create label[i]
    QLabel *label = new QLabel;
    label->setText(QString("Quant"));
    
    // create slider
    m_sliderQ = new QSlider(Qt::Horizontal, m_ctrlGrp);
    m_sliderQ->setTickPosition(QSlider::TicksBelow);
    m_sliderQ->setTickInterval(25);
    m_sliderQ->setMinimum(1);
    m_sliderQ->setMaximum(MXGRAY);
    m_sliderQ->setValue  (1);
    
    // create spinbox
    m_spinBoxQ = new QSpinBox(m_ctrlGrp);
    m_spinBoxQ->setMinimum(1);
    m_spinBoxQ->setMaximum(MXGRAY);
    m_spinBoxQ->setValue  (1);
    
    // init signal/slot connections for quantization
    connect(m_sliderQ , SIGNAL(valueChanged(int)), this, SLOT(changeQuant (int)));
    connect(m_spinBoxQ, SIGNAL(valueChanged(int)), this, SLOT(changeQuant (int)));
    connect(checkbox_dithering, SIGNAL(stateChanged(int)), this, SLOT(check_dither_state (int)));

    // assemble dialog
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(  label  , 0, 0);
    layout->addWidget(m_sliderQ , 0, 1);
    layout->addWidget(m_spinBoxQ, 0, 2);
    layout->addWidget(checkbox_dithering, 1, 1);
    
    // assign layout to group box
    m_ctrlGrp->setLayout(layout);
    
    return(m_ctrlGrp);
}

void
Quantization::changeQuant(int thr)
{
    m_sliderQ ->blockSignals(true);
    m_sliderQ ->setValue    (thr );
    m_sliderQ ->blockSignals(false);
    m_spinBoxQ->blockSignals(true);
    m_spinBoxQ->setValue    (thr );
    m_spinBoxQ->blockSignals(false);
    
    // apply filter to source image; save result in destination image
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}

void
Quantization::check_dither_state(int){
    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}

void
Quantization::quantiz(ImagePtr I1, int q, ImagePtr I2){
    IP_copyImageHeader(I1, I2);
    int w = I1->width();
    int h = I1->height();
    int total = w * h;
    int type;
    ChannelPtr<uchar> p1, p2, endd;
    
    double scale = MXGRAY / q;
    
    int i, lut[MXGRAY];
    for(i=0; i<MXGRAY; ++i) //lut[i] = 0;
    {
        lut[i] = scale * (int) (i/scale);
    }
    bool dither =checkbox_dithering->isChecked();
    if(dither){

        int oscillation = 1;
        int bias = scale/2;
        int rand_value = 0;
        for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
            IP_getChannel(I2, ch, p2, type);
            for(endd = p1 + total; p1<endd; p1++) //*p2++ = lut[*p1++];
            {
                rand_value = ((rand() & 0x7fff)/32767.0)* bias ;
                if(oscillation ==1){
                    *p2++ = CLIP(lut[*p1] + rand_value, 0, MXGRAY);
                    oscillation = -1;
                }
                else{
                    *p2++ = CLIP(lut[*p1] - rand_value, 0, MXGRAY);
                    oscillation = 1;

                }
            }
        }
        
    }

    else{

    for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
        IP_getChannel(I2, ch, p2, type);
        for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
    }
    }
}

void
Quantization::reset(){
    
    m_spinBoxQ->setValue    (1 );
    m_sliderQ->setValue  (1);

    applyFilter(g_mainWindowP->imageSrc(), g_mainWindowP->imageDst());
    
    // display output
    g_mainWindowP->displayOut();
}


