//
//  quantization.hpp
//  qip
//
//  Created by Indrajit on 3/21/16.
//
//

#ifndef quantization_h
#define quantization_h

#include "ImageFilter.h"

class Quantization : public ImageFilter {
    Q_OBJECT
    
public:
    Quantization	(QWidget *parent = 0);		// constructor
    QGroupBox*	controlPanel	();		// create control panel
    bool		applyFilter(ImagePtr, ImagePtr);// apply filter to input to init output
    void		reset		();		// reset parameters
    
protected:
    void quantiz(ImagePtr I1, int q, ImagePtr I2);
    
protected slots:
    //void changeContrast(int);//added by me
    //void changeBright(int);
    void changeQuant(int);
    void check_dither_state(int);
private:
    // brightness/contrast controls
    QSlider		*m_sliderQ ;	// brightness slider
    //QSlider		*m_sliderC ;	// contrast   slider
    QSpinBox	*m_spinBoxQ;	// brightness spin box
    //QSpinBox	*m_spinBoxC;	// contrast   spin box
    QCheckBox   *m_checkBox;

    // widgets and groupbox
    QGroupBox	*m_ctrlGrp;	// groupbox for panel
    QCheckBox *checkbox_dithering;
};

#endif /* quantization_hpp */
